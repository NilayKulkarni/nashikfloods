import tornado.ioloop
import tornado.web
import os
from tornado_cors import CorsMixin
from tornado.escape import json_decode
from libfloods.mongo import *
from libfloods.tweepy import *

class LandingHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("static/landing.html")

class HelpHandler(tornado.web.RequestHandler):
    CORS_ORIGIN = '*'
    CORS_HEADERS = 'Content-Type'
    CORS_METHODS = 'POST'
    def get(self):
        self.render("static/help.html")
    def post(self):
        req_body = json_decode(self.request.body)
        location_name = req_body['location']
        people = req_body['people']
        contact = req_body['contact']
        person_name = req_body['person_name']
        food = req_body['food']
        water = req_body['water']
        rescue = req_body['rescue']
        addHelp(location_name, people, food, water, rescue, contact, person_name)
        tweetHelp(location_name, people, contact, food, water)

def make_app():
    return

if __name__ == "__main__":
    app = tornado.web.Application([
        (r"/", LandingHandler),
        (r"/help", HelpHandler)
    ],
    static_path=os.path.join(os.path.dirname('.'), "static")
    )
    app.listen(80)
    tornado.ioloop.IOLoop.current().start()