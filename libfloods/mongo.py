from pymongo import *

# Setup MongoDB
dbclient = MongoClient(host="ds139715.mlab.com", port=39715)
dbclient.nashikfloods.authenticate('nashikfloods', 'nashikfloods')
db = dbclient.nashikfloods


def addHelp(location, people, food, water, rescue, contact, name):
    db.insert({"location": location, "people": people, "food": food, "water": water, "rescue": rescue, "contact" : contact, "name" : name})
